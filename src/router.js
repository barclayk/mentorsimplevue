import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/about",
      name: "about",
      component: () => import("./views/About.vue")
    },
    {
      path: "/usercreate",
      name: "usercreate",
      component: () => import("./components/User/CreateUser")
    },
    {
      path: "/login",
      name: "login",
      component: () => import("./components/User/Login")
    },
    ,
    {
      path: "/getusers",
      name: "getusers",
      component: () => import("./components/User/GetUser")
    }
  ]
});
