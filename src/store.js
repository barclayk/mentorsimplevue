import router from "@/router";
import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const state = {
  user: "",
  loggedin: false,
  users: [],
  apiEndpoint: "http://0.0.0.0:8000/v1/"
};
const getters = {
  user: state => state.user,
  users: state => state.users,
  loggedin: state => state.loggedin
};
const mutations = {
  chgUser(state, usr) {
    state.user = usr;
  },
  chgUsers(state, usr) {
    state.users = usr;
  },
  chgLoggin(state, st) {
    state.loggedin = st;
  },
  clearUser(state) {
    state.user = "";
    state.users = [];
    state.loggedin = false;
  }
};
const actions = {
  createUser({ state, commit }, payload) {
    try {
      let url = state.apiEndpoint + "user";
      console.log(payload);
      let data = {
        email: payload.email,
        mobile: parseInt(payload.mobile),
        pwd: payload.pwd,
        confirmpwd: payload.confirmpwd
      };
      console.log(data);
      console.log(url);
      axios
        .post(url, data)
        .then(resp => {
          console.log(resp);
          router.push("/");
        })
        .catch(ex => {
          console.log("wrong username or password");
        });
    } catch {
      console.log("error on login");
    }
  },
  login({ state, commit }, payload) {
    let url = state.apiEndpoint + "login";
    let data = {
      email: payload.email,
      pwd: payload.pass
    };
    try {
      axios
        .post(url, data)
        .then(resp => {
          console.log(resp);
          if (resp.data.loggedin == "true") {
            state.loggedin = true;
          }

          state.user = resp.data.user;
          console.log(resp);
          router.push("/");
        })
        .catch(ex => {
          console.log("login failed");
        });
    } catch {
      console.log("login failed");
    }
  },
  getUsers({ state, commit }) {
    let url = state.apiEndpoint + "user";
    axios
      .get(url)
      .then(resp => {
        console.log(resp.data);
        state.users = resp.data;
      })
      .catch(ex => {
        console.log("get users unsuccessful");
      });
  }
};

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions
});
